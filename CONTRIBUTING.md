## Contribute to this project

Thank you for your interest in contributing to this project. 
Contributions includes feature enhancement requests, issue reporting, bug fixing and many more.

This guide details how to contribute to this project in a way that is efficient for everyone.

## Submitting a merge request

We welcome merge requests with fixes and improvements to the code or documentation. 

In the issue tracker, the label `Accepting Merge Requests` denotes issues that we agree is a good idea, and would really like merge requests for.

Issues labeled `Needs Discussion` needs futher discussions. Try to resolve these discussions, and get the label removed, before starting to work on these issues. 

Merge requests should be opened at https://gitlab.com/marcjose/java-gradle-baseproject/merge_requests.
