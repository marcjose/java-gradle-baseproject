package com.tutorials.projects.java.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ExampleTest {

  @Test
  /* default */ void totallyUnneccessaryTest() {
    Assertions.assertTrue(
        "com.tutorials.projects.java.test.ExampleTest".equals(getClass().getName()),
        "Whoops this should never happen!"
    );
  }

}
